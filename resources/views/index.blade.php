<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
  <title>axure文档</title>
  <script>
    if(!window.Promise) {
      document.writeln('<script src="./src/components/es6-promise.min.js"'+'>'+'<'+'/'+'script>');
    }
  </script>
  <link rel="stylesheet" href="/dist/index.css?v=1568882183161">
</head>
<body>
  <div id="example" />
  <!-- <script src="/src/components/react-dom.min.js"></script> -->
  <script src="/dist/shared.js?v=1568882183161"></script>
  <script src="/dist/index.js?v=1568882183161"></script>
</body>
</html>
