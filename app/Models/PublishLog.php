<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class PublishLog extends Model
{
    use Searchable;

    protected $table = "publish_log";

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * 获取模型的索引名称.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'publish_log_index';
    }

    /**
     * 获取模型的索引数据数组
     * 自定义被持久化到搜索索引的数据
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // 自定义数组...

        return $array;
    }

    /**
     * Get the value used to index the model.
     *
     * @return mixed
     */
    /*public function getScoutKey()
    {
        return $this->primaryKey;
    }*/
}
