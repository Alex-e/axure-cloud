CREATE TABLE `publish_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '项目名',
  `save_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '保存在服务器的绝对路径',
  `visit_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '页面访问url',
  `create_time` datetime NOT NULL COMMENT '发布时间',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除，1-是，0-否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='axure发布记录表';