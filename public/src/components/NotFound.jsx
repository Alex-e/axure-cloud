/**
 * @author HY
 * @create 2018.12.29
 * 404等异常页面
 */
import React, { Component } from 'react';
// import Header from './Header';

function NotFound(props) {
    return (
        <div>
            {/* <Header title="提示" /> */}
            <div className="KY-notfound-wrap"></div>
        </div>
    )
}

export default NotFound;
// export default class NotFound extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {

//         };
//     }

//     render() {
//         return (
//             <div>
//                 {/* <Header title="提示" /> */}
//                 <div className="KY-notfound-wrap"></div>
//             </div>
//         )
//     }
// }