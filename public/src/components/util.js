/**
 * @author HY
 * @create 2019.1.16
 */
function formatMoney(s, n = 2) {
    return parseFloat(s + '').toFixed(n).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
};

function ArrayAndObjectDeepClone(o) {
    if (o instanceof Array) {
        var n = [];
        for (var i = 0; i < o.length; ++i) {
            n[i] = ArrayAndObjectDeepClone(o[i]);
        }
        return n;
    } else if (o instanceof Object) {
        var n = {}
        for (var i in o) {
            n[i] = ArrayAndObjectDeepClone(o[i]);
        }
        return n;
    } else {
        return typeof o === 'string' ? escapeText(o) : o;
    }
};

function escapeText(value) {
    if (value) {
        var el = document.createElement('div');
        el.innerHTML = value;
        return el.innerText;
    } else {
        return '';
    }
};

function formatMoneyToFixed(s, n = 2) {
    return parseFloat(s + '').toFixed(n);
};

function is_has_number(number) {
    return number !== '' && number !== undefined && number !== null && number >= 0;
};

function trim(str) {
    if (typeof str === 'string') {
        return str.replace(/(^\s*)|(\s*$)/g, '');
    } else if (str instanceof Array) {
        return str.length;
    } else {
        return str;
    }
};

export {
    formatMoney,
    ArrayAndObjectDeepClone,
    escapeText,
    formatMoneyToFixed,
    is_has_number,
    trim
};