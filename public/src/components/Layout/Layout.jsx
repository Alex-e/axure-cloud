/**
 * @author HY
 * @create 2019.9.19
 */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import CSS from './Layout.less';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

function Layouts(props) {
  const [collapsed, setCollapsed] = useState(false);
  const [selectedKey, setSelectedKey] = useState(window.location.hash.replace('#', ''));

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapsed} onCollapse={(collapsed) => setCollapsed(collapsed)}>
        <div className={CSS['logo']} />
        <Menu theme="dark" defaultSelectedKeys={[selectedKey]} mode="inline">
          <Menu.Item key="/">
            <Link to="/">
              <Icon type="database" />
              <span>列表</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="/upload">
            <Link to="/upload">
              <Icon type="cloud-upload" />
              <span>上传</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }} />
        <Content style={{ margin: '0 16px' }}>
          {/* <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>User</Breadcrumb.Item>
            <Breadcrumb.Item>Bill</Breadcrumb.Item>
          </Breadcrumb> */}
          <div style={{ padding: 24, marginTop: 16, background: '#fff', minHeight: 360 }}>{props.children}</div>
        </Content>
        <Footer style={{ textAlign: 'center' }}></Footer>
      </Layout>
    </Layout>
  )
}

export default Layouts;