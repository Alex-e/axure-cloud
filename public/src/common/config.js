/**
 * @author HY
 * @create 2019.9.19
 */
const HTTP_URL = {
  GET_TABLE_LIST: '/axureList',
  IMPORT_FILE_URL: '/publishAxure',
  DELETE_FILE_URL: '/del'
};

export {
  HTTP_URL
}