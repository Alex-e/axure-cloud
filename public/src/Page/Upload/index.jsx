/**
 * @author HY
 * @create 2019.9.19
 */
import React, { useState, useEffect } from 'react';
import { Input, Button, notification } from 'antd';
import Layouts from '../../components/Layout/Layout';
import CSS from './index.less';
import { HTTP_URL } from '../../common/config';

const $ = require('../../components/jquery.min.js');

function UploadFile() {
  const [cname, setCname] = useState('');
  const [loading, setLoading] = useState(false);

  function beforeUpload(e) {
    const file = e.target.files[0];
    if (!file['name'].match(/\.(zip)$/i)) {
      notification.warning({
        message: '提醒',
        description: '文件格式不正确'
      });
      setCname('');
      return;
    }
    setCname(file['name']);
  }

  useEffect(() => {
    if (loading) {
      $.ajax({
        url: HTTP_URL.IMPORT_FILE_URL,
        type: 'post',
        data: new FormData(document.getElementById('form')),
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        success: function (data) {
          setLoading(false);
          if (data.code == 1) {
            notification.success({
              message: '提醒',
              description: data.info,
              duration: 2,
              onClose() {
                window.location.href = '/';
              }
            });
          } else {
            notification.error({
              message: '提醒',
              description: data.info
            });
          }
        },
        error: function () {
          setLoading(false);
          notification.error({
            message: '提醒',
            description: 'something error'
          });
        }
      });
    }
  }, [loading])

  return (
    <Layouts>
      <div className={CSS['container']}>
        <form id="form">
          <div className={`${CSS['item']} clearfix`}>
            <div className="KY-main-fluid">
              <div className={CSS['content']}>
                <Input name="project_name" />
              </div>
            </div>
            <div className={`KY-l-fluid ${CSS['label']}`}>项目名称</div>
          </div>
          <div className={`${CSS['item']} clearfix`}>
            <div className="KY-main-fluid">
              <div className={CSS['content']}>
                <div className="clearfix">
                  <div className={CSS['upload']}>
                    <Button type="primary">上传</Button>
                    <input type="file" accept="application/zip,application/x-zip,application/x-zip-compressed" name="file" className={CSS['inputbtn']} onChange={(e) => beforeUpload(e)} />
                  </div>
                  <p className={CSS['file']}>{cname}</p>
                </div>
                <p className={CSS['tips']}>仅支持上传zip文件类型</p>
              </div>
            </div>
            <div className={`KY-l-fluid ${CSS['label']}`}>需求文档</div>
          </div>
        </form>
        <div className={CSS['submit']}>
          <Button type="primary" loading={loading} onClick={() => setLoading(true)}>提交</Button>
        </div>
      </div>
    </Layouts>
  )
}

export default UploadFile;