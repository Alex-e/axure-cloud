import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import { Router, Route, hashHistory } from 'react-router';
import { ConfigProvider } from 'antd';
import Bundle from './bundle';
import zhCN from 'antd/es/locale/zh_CN';

import List from './Page/List/index';
import NotFound from './components/NotFound';

import './index.less';
import './custom.less';

const routes = [{
    path: '/',
    component: List
}, {
    path: '/upload',
    component: (props) => (
        <Bundle load={
            (cb) => {
                require.ensure([], require => {
                    cb(require('.//Page/Upload/index').default)
                }, 'upload/index')
            }
        }>
            {(Componet) => <Componet {...props} />}
        </Bundle>
    )
}, {
    path: '*',
    component: NotFound
}];

ReactDOM.render(
    (
        <ConfigProvider locale={zhCN}>
            <Router history={hashHistory}>
                {routes.map((item, index) => <Route key={`item_${index}`} path={item.path} component={item.component} />)}
            </Router>
        </ConfigProvider>

    ),
    document.getElementById('example')
);