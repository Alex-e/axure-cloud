const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const theme = require('./package.json').theme;

const HappyPack = require('happypack');
const os = require('os');
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length - 3 });

const AllExtractTextPlugin = new ExtractTextPlugin({ filename: '[name].css', allChunks: true });
// const FlowExtractTextPlugin = new ExtractTextPlugin({ filename: 'css/common/index.css', allChunks: true });

const postcssOpts = {
    // modifyVars: theme,
    ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
    plugins: () => [
        autoprefixer({
            browsers: ['last 2 versions', 'Firefox ESR', '> 1%', 'ie >= 8', 'iOS >= 8', 'Android >= 4'],
        })
    ],
};

module.exports = {
    devtool: 'source-map', // or 'inline-source-map'
    devServer: {
        disableHostCheck: true
    },

    entry: { 
        "index": path.resolve(__dirname, 'src/index') 
        // "js/flowEngine/index": path.resolve(__dirname, 'src/flowEngine/index')
    },

    output: {
        filename: '[name].js',
        chunkFilename: './js/[name].chunk.js', // ./js/[name]-[hash].chunk.js
        path: path.join(__dirname, './dist'),
        publicPath: './dist/'
    },

    resolve: {
        modules: [path.resolve(__dirname, 'node_modules'), path.join(__dirname, 'src')],
        extensions: ['.web.js', '.jsx', '.js', '.json'],
    },

    module: {
        rules: [{
            test: /\.js[x]?$/,
            exclude: /node_modules/,
            use: ['happypack/loader?id=babel']
        },
        {
            test: /\.(jpg|png|gif)$/, loader: "url-loader", options: {
                limit: 8192,
                name: 'assets/[name].[hash:8].[ext]',
            }
        },
        {
            test: /\.less$/i,
            exclude: /node_modules/,
            use: AllExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    { loader: 'css-loader', options: { modules: true, localIdentName: '[local]-[hash:base64:6]' } },
                    { loader: 'postcss-loader', options: postcssOpts },
                    'less-loader'
                ]
            })
        },
        // {
        //     test: /src\/flowEngine\index\.less/,
        //     exclude: /node_modules/,
        //     use: FlowExtractTextPlugin.extract({
        //         fallback: 'style-loader',
        //         use: [
        //             { loader: 'css-loader', options: { modules: true, localIdentName: '[local]-[hash:base64:6]' } },
        //             { loader: 'postcss-loader', options: postcssOpts },
        //             'less-loader'
        //         ]
        //     })
        // },
        {
            test: /\.less$/i,
            include: /node_modules/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    'css-loader',
                    { loader: 'postcss-loader', options: postcssOpts },
                    { loader: 'less-loader', options: { modifyVars: theme, javascriptEnabled: true } }
                ]
            })
        },
        {
            test: /\.css$/i,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    'css-loader', { loader: 'postcss-loader', options: postcssOpts }
                ]
            })
        }
        ]
    },
    // externals: {
    //     "react": "React",
    //     "react-dom": "ReactDOM"
    // },
    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new HappyPack({
            // 用唯一的标识符 id 来代表当前的 HappyPack 是用来处理一类特定的文件
            id: 'babel',
            threadPool: happyThreadPool,
            // 如何处理 .js 文件，用法和 Loader 配置中一样
            loaders: [{
                loader: 'babel-loader',
                options: {
                    cacheDirectory: true,
                    presets: ['es2015', 'stage-1', 'react'],
                    plugins: [
                        'external-helpers', // why not work?
                        ["transform-runtime", { polyfill: false }],
                        ["import", { "style": true, "libraryName": "antd" }]
                    ]
                }
            }]
        }),
        // new webpack.optimize.CommonsChunkPlugin('shared.js'),
        new webpack.optimize.CommonsChunkPlugin({
            // minChunks: 2,
            name: 'shared',
            filename: 'shared.js'
        }),
        AllExtractTextPlugin
        // FlowExtractTextPlugin
        // new ExtractTextPlugin({ filename: '[name].css', allChunks: true })
    ]
}