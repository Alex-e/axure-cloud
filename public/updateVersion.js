/**
 * @author HY
 * @create 2019.1.22
 * 更新版本号
 */
var fs = require('fs'); //文件读写
var configPath = '../resources/views/index.blade.php';
//剔除其他项目的配置文件
updateProjectName(configPath, function (text) {
    fs.writeFile(configPath, text, { flag: 'w', encoding: 'utf-8', mode: '0666' }, function (err) {
        if (err) {
            console.log('文件写入失败')
        } else {
            console.log('文件写入成功');
        }
    })
})
//修改config的配置
function updateProjectName(filePath, suc) {
    var reg = /(\.css|\.js)\?v=\d+/g;
    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) {
            throw err;
        }
        var time = (new Date()).getTime();
        var text = data.replace(reg, '$1?v=' + time);
        suc(text);
    });
}